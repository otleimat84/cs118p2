#pragma once
#include <vector>
#include <istream>
#include <ostream>
#include <utility>
#include <stdint.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <list>
#include <queue>
#include <vector>
#include <algorithm>
#include <unistd.h>
#include "packet.h"
#include <string>
#include "constants.h"


class ConnectionHandler {
	public:
		ConnectionHandler(int sockFd, bool client);
		~ConnectionHandler(){}
		ConnectionHandler(){}
		// NEED TO DO
		void readData(std::basic_ostream<char>& os, size_t size);
		void writeData();
		// FILE TRANSFER SHTUFF
   		std::string getFileName();
    	void sendFileName(const std::string fileName){
    		int x;
    	}
    	void fileNotFound();
    	void transferFile(std::basic_istream<char>& is, size_t size);
    	void receiveFile(std::basic_ostream<char>& os);


	private:
		void clientHandshake(void);
		void serverHandshake(void);
		bool incrementing(Packet packet, PACKET_TYPE type);
		void setTimeout(int sec, int uSec);
		bool isClient(){return m_isClient;}
		
		bool m_isClient;
		bool m_isEstablished;
		uint32_t m_sendBase, m_recBase;
		int m_nextSeqNum;
		int m_sockFd;
		struct sockaddr_in m_cli_addr;
		socklen_t m_cli_len;
		Packet* firstPacket = NULL;
		std::list<std::pair<Packet, uint64_t>> m_receivedPackets;
		
};


