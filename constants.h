// Constants.h
// Contains all useful constants to be used likely in multiple files
#if !defined(MYCONSTANTS_H)
#define MYCONSTANTS_H 1

enum PACKET_TYPE {NONE, SYN, ACK, SYNACK, FIN};
const uint32_t MaxPacketSize = 1024;
const uint32_t MaxSequenceNumber = 30720;
const uint32_t WindowSize = 5120;
const uint32_t HeaderSize = 10;
// Milliseconds
const uint32_t RTO = 500;
// Microseconds
const uint32_t RTO_us = 500000;
// Seconds
const uint32_t MaxFinishRetryTimeValue = 1;

const uint32_t InitialSlowStartThreshold = 15360;
const uint32_t InitialCongestionWindowSize = 1024;


#endif
