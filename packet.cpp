
#include "packet.h"

//In Spec default values:
#define MAX_PACKET_LENGTH 1024 //bytes
#define MAX_SEQ_NUM 30720 //bytes
#define WND 5120 //bytes


#define BYTEBITMASK 0xFF
#define TWOBYTEBITMASK 0xFFFF
#define HEADER_SIZE 10 //bytes

/**
//??????
#define PACKETSIZE 576 
#define MAXDATASIZE 560
//??????
**/

#define PACKETSIZE 1024
#define MAXDATASIZE PACKETSIZE - HEADER_SIZE
#define SYNBITMASK 0b010
#define ACKBITMASK 0b100
#define SYNACKBITMASK 0b110
#define FINBITMASK 0b001

//HEADER INCLUDEES THE FOLLOWING
//16-bit sequence number
//16-bit ACK number
//16-bit packet size (in bytes)
//16-bit WindowSize (constant)
//3-bit Flag field (ACK, SYN, FIN)
//8-bit Checksum



uint8_t Packet::CreateChecksum( void ){
  int checksum = 0;
  int size = _size;
  
  for (int i = 0; i < size; i++){
    checksum += _raw_data[i];
    if (checksum & 0b100000000) //If overflow bit is set
      checksum++;
    checksum = (checksum & BYTEBITMASK);
  }

  checksum = ~checksum;

  return checksum;
}

bool Packet::VerifyChecksum( void ){
  int checksum = 0;
  int size = _size;

  for (int i = 0; i < size; i++){
    checksum += _raw_data[i];
    if (checksum & 0b100000000) //If overflow bit is set
      checksum++;
    checksum = (checksum & BYTEBITMASK);
  }

  if (checksum == 0b11111111)
    return true;

  return false;
}

uint8_t* Packet::GetRawData ( void ){
  uint8_t *raw = (uint8_t *)calloc(sizeof(uint8_t), _size);
  for (int i = 0; i < _size; i++)
    raw[i] = _raw_data[i];

  return raw;
}

void Packet::SetRawData ( uint8_t *raw ){
  for (int i = 0; i < _size; i++)
    _raw_data[i] = raw[i];

  for (int i = HeaderSize; i < _size; i++)
    _data[i - HeaderSize] = raw[i];

}

//Constructor used for RECIEVING raw data from another host
Packet::Packet(uint8_t raw []){
   //Source
 
  _seq = (raw[0] << 8) + raw[1];
  _ack = (raw[2] << 8) + raw[3];
  _wnd = (raw[4] << 8) + raw[5];
  _size = (raw[6] << 8) + raw[7];
  _checksum = raw[9];
  
  if ((raw[8] & SYNACKBITMASK) == SYNACKBITMASK)
    _type = SYNACK;
  else if ((raw[8] & SYNBITMASK) == SYNBITMASK)
    _type = SYN;
  else if ((raw[8] & ACKBITMASK) == ACKBITMASK)
    _type = ACK;
  else if ((raw[8] & FINBITMASK) == FINBITMASK)
    _type = FIN;
  else
    _type = NONE;

  SetRawData(raw);
  
}

//Constructor used for CREATING new packets for another host
Packet::Packet(uint32_t seq_num, uint32_t ack_num, PACKET_TYPE type,
	       uint8_t data [], int data_size){
  int packet_size = data_size + HEADER_SIZE;

  //Fill in raw data
  bzero(_raw_data, PACKETSIZE);
  
  //Sequence Number
  _raw_data[0] = (seq_num >> 8) & BYTEBITMASK;
  _raw_data[1] =  seq_num       & BYTEBITMASK;

  //Acknowledgement Number
  _raw_data[2] = (ack_num >> 8) & BYTEBITMASK;
  _raw_data[3] =  ack_num       & BYTEBITMASK;

  //Window Size
  _raw_data[4] = (WND >> 8) & BYTEBITMASK;
  _raw_data[5] =  WND       & BYTEBITMASK;

  //Packet Size
  _raw_data[6] = (packet_size >>  8) & BYTEBITMASK;
  _raw_data[7] =  packet_size        & BYTEBITMASK;

  //Packet Type
  if (type == ACK)
    _raw_data[8] = 0b100;
  else if (type == SYN)
    _raw_data[8] = 0b010;
  else if (type == SYNACK)
    _raw_data[8] = 0b110;
  else if (type == FIN)
    _raw_data[8] = 0b001;
  else
    _raw_data[8] = 0b000;
  

  //PUT IN DATA
  for (int i = 0; i < data_size; i++){
    _raw_data[i + HeaderSize] = data[i];
    _data[i] = data[i];
  }
  
  //Calculate Checksum
  _size = packet_size;
  _checksum = CreateChecksum();
  _raw_data[9] =  _checksum;

  //Put in other private member variables
  _seq = seq_num;
  _ack = ack_num;
  _wnd = WND;
  _type = type;


}
/**
int main (void){
  uint8_t hold [PACKETSIZE];
  uint8_t omar [200] = "Hi Omar";
  Packet p(101, 202, SYNACK, omar, 7);
  //  p.GetRawData(hold);
  std::cout << p.GetRawData() + HEADER_SIZE << std::endl;
  Packet k(p.GetRawData());

  std::cout << k.IsValid();

  return 0;
}

**/
