#include <vector>
#include <iterator>
#include <chrono>
#include <iostream>
#include <stdint.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "RDTP.h"
using namespace std;


ConnectionHandler::ConnectionHandler(int sockFd, bool client){
	m_sockFd = sockFd;
	m_isClient = client;
	m_cli_len = sizeof(m_cli_addr);
	m_isEstablished = true;
	isClient() ? clientHandshake(): serverHandshake();
}

void ConnectionHandler::setTimeout(int sec, int microsec){
	struct timeval tv;
	tv.tv_sec = sec;
	tv.tv_usec = microsec;
	setsockopt(m_sockFd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));

}


bool ConnectionHandler::incrementing(Packet packet, PACKET_TYPE type){
	if (packet.GetType() == type){
		m_recBase = packet.GetAck() + 1;
		return true;
	}
	return false;
}

void ConnectionHandler::printPacket(){
  
}

void ConnectionHandler::clientHandshake(){
	cout << "Starting client Handshake" << endl;
	int length;
	m_recBase = 0;
	m_nextSeqNum = 0;
	uint8_t buffer[MaxPacketSize];
	bool retransmit = false;
	setTimeout(0, RTO_us);
	Packet p = Packet(0, 0, SYN, 0, 0);
	while (1){
		write(m_sockFd, p.GetRawData(), MaxPacketSize);
		length = recvfrom(m_sockFd, buffer, MaxPacketSize, 0, (struct sockaddr*)&m_cli_addr, &m_cli_len);
		if (length > 0)cout << "Length: " << length << endl;
		if (length <= 0){
		  cout << "Received: " << length << endl;
		  perror("recvfrom had an error");
		  retransmit = true;
		  continue;
		}
		Packet packet = Packet(buffer);
		if (incrementing(packet, SYNACK))
			break;

		m_sendBase += 1;
		m_nextSeqNum += 1;
	}
	cout << "yeup" << endl;

}

void ConnectionHandler::serverHandshake(){
  int length;
  m_recBase = 0;
  m_nextSeqNum = 0;
  uint8_t buffer[MaxPacketSize];
  bool retransmit = false;
  cout << "Starting server Handshake" << endl;

  setTimeout(0, 0);

  while (1){
    length = recvfrom(m_sockFd, buffer, MaxPacketSize, 0, (struct sockaddr*)&m_cli_addr, &m_cli_len);
    cout << "Printing Length" << endl;
    if (length > 0){			
      cout << length << endl;
      for (int i = 0; i < length; i++){
	cout << buffer[i];
      }
      Packet packet = Packet(buffer);

      cout << "Checking if SYN" << endl;
      if (incrementing(packet, SYN))
	break;
      else {
	cout << "Checking if FIN" << endl;
	if (packet.GetType() == FIN){
	  Packet ack = Packet(m_nextSeqNum, packet.GetAck(), ACK, 0, 0);
	  sendto(m_sockFd, ack.GetRawData(),
		 ack.GetDataSize(),0,
		 (struct sockaddr*)&m_cli_addr, m_cli_len);
	  continue;
	}
	else {
	  perror("expected something else");
	  exit(0);
	}

      }
    }
			
  }

  cout << "Got SYN" << endl;
  
  setTimeout(0, RTO_us);
  Packet p = Packet(0, m_nextSeqNum,  SYNACK, nullptr,0);
  retransmit = false;
  

  sendto(m_sockFd, p.GetRawData(),
		 p.GetDataSize(),0,
		 (struct sockaddr*)&m_cli_addr, m_cli_len);
	  
  //write(m_sockFd, p.GetRawData(), MaxPacketSize);


  cout << "Sent Packet" << endl;
  
  while (1){
    length = recvfrom(m_sockFd, buffer, MaxPacketSize, 0, (struct sockaddr*)&m_cli_addr, &m_cli_len);
    cout << "Trying to receive" << endl;
    if (length > 0)
      {
	Packet packet = Packet(buffer);
	if (packet.GetType() == SYN){
	  continue;
	}
	else if (packet.GetType() != ACK){
	  firstPacket = new Packet(packet);
	  break;
	}
	else {break;}

      }
  }
  m_sendBase += 1;
  m_nextSeqNum += 1;
}
/*receiver:
        while true
            block until packet is received
            if rcvbase - window_size <= seq <= rcvbase - 1
                ACK(seq)
            else if rcvbase <= seq <= rcvbase + window_size - 1
                ACK(seq)
                insert into buffer list which is ordered by sequence
                while rcvbase == left most entry of list
                    write packet data
                    update rcvbase += size of packet
                    remote entry from list
            else
                do nothing*/
void ConnectionHandler::readData(std::basic_ostream<char>& os, size_t size){
	ostream_iterator<char> osi(os);
	size_t bytesRead = 0;

}


