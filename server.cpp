#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <cassert>
#include <sstream>
#include <string>
#include <iterator>
#include <iomanip>

#include "RDTP.h"

using namespace std;
struct hostent *host;
char* hostName;
int sockfd, portno;
struct sockaddr_in serv_addr;
string fileName;

// bool getStream(string & fileName, std::basic_ostream<uint8_t>& fileStream){
//   fileStream.open(filename.c_string());

//   if(!fileStream.is_open()){
//     fileStream.open("404.html");
//     return false;
// //   }

//   return true;
// }


void createSocket(){

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    if (sockfd < 0)
        perror("ERROR opening socket");

    memset(&serv_addr, '\0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
      printf("ERROR on binding");
    
    //Not needed for Connectionless UDP
    /*
    listen(sockfd,5);
        
    while(accept(sockfd,(struct sockaddr *) &serv_addr,
	       (unsigned int *) sizeof(serv_addr)) < 0){
      perror ("Couldn't accept");
    }
    **/
}

int main(int argc, char** argv)
{
  std::basic_ostream<uint8_t> fileStream(NULL);
  std::string usg_msg = "USAGE: server <portnumber>\n";

    if (argc != 2)
    {
      fprintf(stderr, usg_msg.c_str());
      exit(1);
    }
    
    portno = atoi(argv[1]);
    printf("Creating Socket\n");
    createSocket();
    printf("Created Socket\n");
    ConnectionHandler* ch = new ConnectionHandler(sockfd, false);
    
    close(sockfd);
    return 0;
}

