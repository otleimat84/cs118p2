#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>

#include "RDTP.h"


using namespace std;
struct hostent *host;
char* hostName;
int portno, sockfd;
struct sockaddr_in serv_addr;
void makeConnection();
vector<char> formatFileName(string& fileName);

int main(int argc, char** argv)
{
  std::string usg_msg = "USAGE: client <server hostname> <server portnumber> <file name>\n";

    if (argc != 4)
    {
      fprintf(stderr, usg_msg.c_str());
        exit(1);
    }
    hostName = argv[1];
    portno = atoi(argv[2]);
    makeConnection();
    string fileName = argv[3];
    // do something with filename
    ConnectionHandler* rdtp = new ConnectionHandler(sockfd, true);
    rdtp->sendFileName(fileName);
    //rdtp.sendFileName(formatFileName(fileName));
    

}


void makeConnection(){
    host = gethostbyname(hostName);
    if (!host)
    {
        fprintf(stderr, "Host error%s\n", hostName);
        exit(1);
    }
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        perror("ERROR opening socket");
    memset(&serv_addr, '\0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    memcpy((void *)&serv_addr.sin_addr, host->h_addr_list[0], host->h_length);
    serv_addr.sin_port = htons(portno);

    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        perror("Connection error");
}

