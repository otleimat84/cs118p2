CXX = g++
CPPFLAGS = -Wall -Wextra -march=native -mtune=native -g -static-libstdc++ -std=gnu++11
BUILD_DIR = .
COMMON_CPP = packet.cpp RDTP.cpp
CLIENT_CPP = client.cpp
SERVER_CPP = server.cpp
COMMON_OBJ = $(COMMON_CPP:%.cpp=$(BUILD_DIR)/%.o)
CLIENT_OBJ = $(CLIENT_CPP:%.cpp=$(BUILD_DIR)/%.o)
SERVER_OBJ = $(SERVER_CPP:%.cpp=$(BUILD_DIR)/%.o)
COMMON_DEP = $(COMMON_OBJ:%.o=%.d)
CLIENT_DEP = $(CLIENT_OBJ:%.o=%.d)
SERVER_DEP = $(SERVER_OBJ:%.o=%.d)

default: build_dir client server

build_dir: $(BUILD_DIR)

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

client: $(CLIENT_OBJ) $(COMMON_OBJ)
	$(CXX) $(CPPFLAGS) $^ -o $@

server: $(SERVER_OBJ) $(COMMON_OBJ)
	$(CXX) $(CPPFLAGS) $^ -o $@

-include $(COMMON_DEP) $(CLIENT_DEP) $(SERVER_DEP)
