#ifndef PACKET_H
#define PACKET_H
#include <iostream>
#include <stdint.h>
#include <sys/types.h>
#include <strings.h>
#include "constants.h"

class Packet{

private:
  //Helper Functions
  void    SetRawData     ( uint8_t *raw );
  uint8_t CreateChecksum ( void );
  bool    VerifyChecksum ( void );

  //Private member variables
  uint8_t      _raw_data [MaxPacketSize];
  uint16_t     _seq;
  uint16_t     _ack;
  uint16_t     _wnd;
  uint16_t     _size;
  PACKET_TYPE  _type;
  uint8_t      _checksum;
  uint8_t      _data [MaxPacketSize - HeaderSize];
 public:

  //Constrcutors
  Packet( void ) {}
  Packet( uint32_t seq_num, uint32_t ack_num, PACKET_TYPE type, uint8_t data [], int data_size);
  Packet( uint8_t raw []);
  
  //Public Functions
  PACKET_TYPE GetType      ( void ) { return _type; }
  uint16_t    GetSeq       ( void ) { return _seq; }
  uint16_t    GetAck       ( void ) { return _ack; }
  bool        IsValid      ( void ) { return VerifyChecksum(); }
  uint8_t *   GetRawData   ( void );
  uint16_t    GetDataSize(){return _size;}


};

#endif
